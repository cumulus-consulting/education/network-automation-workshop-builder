#!/usr/bin/env python3
"""
:)


"""
import os
import json
import sys
import re
from air_sdk import AirApi
from datetime import datetime, timedelta

def get_time_delta(hours):
    return str(datetime.now() + timedelta(hours=hours))

def get_dot_file_str(rel_path):
    fd = open(rel_path, "r") # import the .dot file into a string
    if fd.mode == 'r':
        dotfile = fd.read()

    return dotfile

def create_topology(air, dotfile_name, org, ci_pipeline_id):
    dotfile = get_dot_file_str(dotfile_name)

    #debug output
    print("Dotfile dump after reading from file. before create_topology call")
    print(dotfile)
    print("")

    # embed project name in here
    topo_name = "Cumulus In The Cloud Demo"
    metadata = '{"citc":"true"}'
    diagram_url = "/images/blank_slate_topology.svg"

    try:
        topology = air.topologies.create(dot=dotfile)
    except:
        print("Unexpected response trying to create a new topology from the .dot file")
        sys.exit(1)

    print("Topology created. dump:")
    print("")
    print(topology.__dict__)
    print("")

    try:
        topology.update(name=topo_name, organization=org, metadata=metadata, diagram_url=diagram_url)
    except:
        print("Unexpected response updating topology")
        sys.exit(1)

    #debug output
    print("Topology updated. dump:")
    print("")
    print(topology.__dict__)
    print("")

    return topology

def create_simulation(air, topology, org, ci_pipeline_id):
    expires = "true"
    sleep = "false"
    expires_at = get_time_delta(24)
    name = topology.name + ":base-simulation:" + ci_pipeline_id
    documentation = "https://gitlab.com/cumulus-consulting/air/guided-tour/-/raw/master/README.md"
    diagram_url = "/images/blank_slate_topology.svg"
    metadata = '{"power_off": false, "delete": false}'

    # debug output
    print(f'Sim Create Variable Check - Before Create:')
    print(f'topology= {topology.url}')
    print(f'name= {name}')
    print(f'sleep= {sleep}')
    print(f'expires= {expires}')
    print(f'expires_at= {expires_at}')
    print(f'title= {name}')
    print(f'organization= {org}')
    print(f'documentation= {documentation}')
    print(f'diagram_url= {diagram_url}')
    print(f'metadata= {metadata}')
    print("")

    try:
        simulation = air.simulations.create(topology=topology, name=name, sleep=sleep, expires=expires, diagram_url=diagram_url,
                     expires_at=expires_at, title=name, organization=org, documentation=documentation, metadata=metadata)
    except:
        print("Unexpected response trying to create a new simulation from the topology object")
        sys.exit(1)

    #debug output
    print("Simulation create success.")
    print("Simulation object dump:")
    print("")
    print(simulation.__dict__)
    print("")

    #set_permissions(simulation)
    #print("Done setting permissions on simulation")

    return simulation

def set_permissions(topo_or_simulation):
    with open('email-permissions.txt') as f:
        emails = f.read().splitlines()

    for email in emails:
        print ("Enabling permission for: " + email)
        try:
            topo_or_simulation.add_permission(email, write_ok=True)
        except:
            print("Failed while adding permissions for: " + email)

def write_artifact_file(simulation, oob_server_node, netq_ts_node):
    data_artifact = {}
    data_artifact["sim_id"] = simulation.id
    data_artifact["netq_node_id"] = netq_ts_node.id
    data_artifact["oob_server_id"] = oob_server_node.id

    print(json.dumps(data_artifact, indent=4))
    print("Writing artifact file...")
    with open('data_artifact.json', 'w') as outfile:
        json.dump(data_artifact, outfile)

def get_simulation_node(air, simulation, nodestr):
    try: # get simulation node
        simulation_node = air.simulation_nodes.list(simulation=simulation.id, name=nodestr)
        if len(simulation_node) > 1:
            print (f'More than one simulation node returned by sim.id and name {nodestr}')
            sys.exit(1)
        simulation_node = simulation_node[0]
    except:
        print(f'Unexpected error while getting the simulation-node for {simulation_node}')
        raise

    return simulation_node

def del_instructions(node):
    try:
        node.instructions.delete()
        #air.delete(node["url"] + "instructions/")
        print(f'Deleted instructions for: {node}')
    except:
        print(f'Error while deleting the instructions for: {node}')
        raise

def send_file_executor_instruction(node, data):
    try:
        node.create_instructions(data=json.dumps(data), executor='file')
        print(f'created instruction for: {node}')
    except:
        print(f'Error while creating instruction for: {node}')
        raise

def drop_file_with_file_executor(node, src_filename, target_filename, post_cmd):
    data = {}

    fd = open(src_filename, "r")
    if fd.mode == 'r':
        file_str = fd.read()

    data[target_filename] = file_str
    data["post_cmd"] = post_cmd

    # debugging
    print(f'{src_filename} instruction data is:')
    print(data)

    send_file_executor_instruction(node, data)

if __name__ == '__main__':

    #check for staging
    if "staging" in os.getenv('CI_COMMIT_REF_NAME'):
        print ("ON staging branch. Directing API and org to staging")
        AIR = AirApi(api_url='https://staging.air.cumulusnetworks.com/api/', username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))
        ORGANIZATION = "https://staging.air.cumulusnetworks.com/api/v1/organization/70850521-37a2-4ab9-9f76-1b3508c01d36/"
    else:
        print ("Didn't detect staging branch. Using AIR prod")
        AIR = AirApi(username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))
        ORGANIZATION = "https://air.cumulusnetworks.com/api/v1/organization/66c9ed73-d076-4e1a-858d-61667bb60c5e/"

    # uniqueness and version tracking
    CI_PIPELINE_ID = os.getenv('CI_PIPELINE_ID')

    # create topology from dot file
    TOPOLOGY = create_topology(AIR, "topology.dot", ORGANIZATION, CI_PIPELINE_ID)
    
    # create simulation from topology
    SIMULATION = create_simulation(AIR, TOPOLOGY, ORGANIZATION, CI_PIPELINE_ID)

    # fetch the simulation_nodes
    OOB_NODE = get_simulation_node(AIR, SIMULATION, "oob-mgmt-server")
    NETQ_TS_NODE = get_simulation_node(AIR, SIMULATION, "netq-ts")

    # add extras to netq-ts for this base topology 
    #drop_file_with_file_executor(NETQ_TS_NODE, "helper_files/netq-ts-customizations.sh", "/home/cumulus/customizations.sh", ["bash /home/cumulus/customizations.sh", "rm /home/cumulus/customizations.sh"])

    # add extras to oob-server this base topology 
    drop_file_with_file_executor(OOB_NODE, "helper_files/oob-mgmt-server-customizations.sh", "/home/cumulus/customizations.sh", ["bash /home/cumulus/customizations.sh", "rm /home/cumulus/customizations.sh"])

    # Drop in ZTP script on oob-mgmt-server
    drop_file_with_file_executor(OOB_NODE, "helper_files/cumulus-ztp", "/var/www/html/cumulus-ztp", ["chmod 755 /var/www/html/cumulus-ztp"])
   
    # start the simulation
    SIMULATION.start()

    # write out the simulation ID to a file as an artifact available in future jobs
    write_artifact_file(SIMULATION, OOB_NODE, NETQ_TS_NODE)

    print("Simulation started!")
    # print more output and info here
