#!/usr/bin/env python3 -u
"""
:)


"""
import os
import json
import sys
import time
from air_sdk import AirApi

def get_artifact(artifact_name):
    artifact = {}
    with open(artifact_name) as json_file:
        artifact = json.load(json_file)

    return artifact

def wait_for_netq(netq_node):
    while True:
        instructions = netq_node.list_instructions()
        print("In loop. Instructions:")
        print(instructions)
        if not instructions:
            print("if not instructions was true. looks done. break out of loop and move on")
            break
        else:
            print("if not instructions was false. So it looks like the instructions are still here. waiting 1 min and retry")
            time.sleep(60)

if __name__ == '__main__':
    #check for staging
    if "staging" in os.getenv('CI_COMMIT_REF_NAME'):
        print ("ON staging branch. using air staging")
        AIR = AirApi(api_url='https://staging.air.cumulusnetworks.com/api/', username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))
    else:
        print ("Didn't detect staging branch. Using air prod")
        AIR = AirApi(username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))

    ARTIFACT = get_artifact('data_artifact.json')

    NETQ_NODE = AIR.simulation_nodes.get(ARTIFACT["netq_node_id"])

    print(f'get netq simulation node complete {NETQ_NODE}')

    print("Entering wait_for_netq from main")
    # waits until the netq node instructions are empty: assumes sim is loaded up.
    wait_for_netq(NETQ_NODE)

    print("Detected empty instrctions for NetQ. Finished loading.")

