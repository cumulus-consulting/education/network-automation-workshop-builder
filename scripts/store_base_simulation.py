#!/usr/bin/env python3
"""
:)

"""
import os
import json
import sys
from air_sdk import AirApi

def get_simulation_id(artifact_name):
    artifact = {}
    with open(artifact_name) as json_file:
        artifact = json.load(json_file)

    return str(artifact["sim_id"])

if __name__ == '__main__':
    if "staging" in os.getenv('CI_COMMIT_REF_NAME'):
        print ("ON staging branch. using air staging")
        AIR = AirApi(api_url='https://staging.air.cumulusnetworks.com/api/', username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))
    else:
        print ("Didn't detect staging branch. Using air prod")
        AIR = AirApi(username=os.getenv('AIR_USERNAME'), password=os.getenv('AIR_PASSWORD'))


    # pull in simulation id string from artifact (previous jobs)
    SIMULATION_ID = get_simulation_id("data_artifact.json")

    # fetch the simulation object
    SIMULATION = AIR.simulation.get(SIMULATION_ID)

    # call action to store the simulation
    SIMULATION.store()

    print("Simulation Storing...")

    SIMULATION.update(expires="false", name='Cumulus In The Cloud Demo')

    print("Simulation expiry disabled")
